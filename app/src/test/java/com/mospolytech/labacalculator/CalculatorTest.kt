package com.mospolytech.labacalculator

import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.IOException

class CalculatorTest {
    var calc: Calculator = Calculator()

    @Test
    @Throws(Exception::class)
    fun calculatePlus() {
        calc.first = 1;
        calc.second = 2;
        calc.operation = "+"
        assertEquals(3, calc.calculate())
}

    @Test
    @Throws(Exception::class)
    fun calculateMinus() {
        calc.first = 7;
        calc.second = 2;
        calc.operation = "-"
        assertEquals(5, calc.calculate())
    }

    @Test
    @Throws(Exception::class)
    fun calculateIncrease() {
        calc.first = 3;
        calc.second = 2;
        calc.operation = "*"
        assertEquals(6, calc.calculate())
    }

    @Test
    @Throws(Exception::class)
    fun calculateDivide() {
        calc.first = 8;
        calc.second = 2;
        calc.operation = "/"
        assertEquals(4, calc.calculate())
    }

    @Test(expected = AssertionError::class)
    @Throws(Exception::class)
    fun calculateDivideZero() {
        calc.first = 1;
        calc.second = 2;
        calc.operation = "/"
        assertEquals(2, calc.calculate())
    }

    @Test(expected = IOException::class)
    @Throws(Exception::class)
    fun calculateUnknownOperation() {
        calc.first = 1;
        calc.second = 2;
        calc.operation = "%"
        assertEquals(3, calc.calculate())
    }
}