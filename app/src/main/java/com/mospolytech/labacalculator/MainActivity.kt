package com.mospolytech.labacalculator

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var calculator: Calculator = Calculator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val list = mutableListOf<Button>()
        list.add(button0);
        list.add(button1);
        list.add(button2);
        list.add(button3);
        list.add(button4);
        list.add(button5);
        list.add(button6);
        list.add(button7);
        list.add(button8);
        list.add(button9);
        list.add(buttonPlus);
        list.add(buttonMinus);
        list.add(buttonCalc)
        list.add(buttonClear)
        list.add(buttonDivide)
        list.add(buttonIncrease)

        val iterator = list.iterator()
        iterator.forEach { button ->
            println("test")
            button.setOnClickListener {
                pushButton(button.text.toString());
            }
        }
    }

    private fun pushButton(sign: String) {
        if (sign == "C") {
            result.setText("0");
            calculator.first = 0;
            calculator.second = 0;
            calculator.operation = "";
            return;
        }
        if ("0123456789".contains(sign)) {
            val text = result.text.toString().toInt()
            if (calculator.second == text) {
                calculator.second = text + sign.toInt();
            } else {
                calculator.second = sign.toInt()
            }
            result.setText(calculator.second.toString())
        } else {
            calculator.first = calculator.calculate();
            result.setText(calculator.first.toString())
            calculator.operation = sign;
            calculator.second = 0;
        }
    }
}
