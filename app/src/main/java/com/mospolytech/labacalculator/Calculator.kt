package com.mospolytech.labacalculator

import java.io.IOException

class Calculator {
    var first = 0
    var operation = ""
    var second = 0

    init {

    }

    fun calculate(): Int {
        if (operation == "C")
            return 0
        if (operation == "+")
            return first + second
        if (operation == "-")
            return first - second
        if (operation == "*")
            return first * second
        if (operation == "/")
            return first / second
        if (operation == "=")
            return first
        if (operation.isEmpty())
            return second
        throw IOException()
    }
}
